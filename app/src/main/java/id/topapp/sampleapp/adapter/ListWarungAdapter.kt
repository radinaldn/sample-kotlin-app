package id.topapp.sampleapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.topapp.sampleapp.R
import id.topapp.sampleapp.model.Warungs
import kotlinx.android.synthetic.main.item_row_warung.view.*

class ListWarungAdapter(private val listWarung: List<Warungs>) : RecyclerView.Adapter<ListWarungAdapter.ListViewHolder>() {

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(warung: Warungs){
            with(itemView){
                Glide.with(itemView.context)
                    .load(warung.pic)
                    .apply(RequestOptions().override(100, 100))
                    .into(img_item_warung)

                tv_warung_name.text = warung.name
                tv_warung_lat.text  = "Lat : "+warung.lat.toString()
                tv_warung_lng.text = "Long : "+warung.lng.toString()
                tv_warung_jarak.text = "Jarak : "+warung.jarak+" Meter"
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_row_warung, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int = listWarung.size

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listWarung[position])
    }
}