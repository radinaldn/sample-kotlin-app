package id.topapp.sampleapp.model

import com.google.gson.annotations.SerializedName

data class Warungs (
    @SerializedName("id") val id : Int? = null,
    @SerializedName("name") val name : String? = null,
    @SerializedName("lat") val lat : Double? = null,
    @SerializedName("lng") val lng : Double? = null,
    @SerializedName("pic") val pic : String? = null,
    var jarak : String? = null

)