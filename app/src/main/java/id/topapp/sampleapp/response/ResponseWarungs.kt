package id.topapp.sampleapp.response

import com.google.gson.annotations.SerializedName
import id.topapp.sampleapp.model.Warungs

data class ResponseWarungs (
    @SerializedName("warungs") val warungs : List<Warungs>
)