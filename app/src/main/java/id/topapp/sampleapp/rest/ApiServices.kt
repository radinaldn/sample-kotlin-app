package id.topapp.sampleapp.rest

import id.topapp.sampleapp.response.ResponseWarungs
import retrofit2.Call
import retrofit2.http.GET

interface ApiServices {
    @GET("api.json")
    fun getAllWarung(): Call<ResponseWarungs>
}