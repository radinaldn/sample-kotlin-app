package id.topapp.sampleapp.activity

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import id.topapp.sampleapp.R
import id.topapp.sampleapp.adapter.ListWarungAdapter
import id.topapp.sampleapp.model.Warungs
import id.topapp.sampleapp.response.ResponseWarungs
import id.topapp.sampleapp.rest.ApiMain
import kotlinx.android.synthetic.main.activity_maps.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var circle: Circle? = null
    private val pekanbaru = LatLng(0.510465, 101.449262) // define pekanbaru location point
    private val radius = 300 // define radius here

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        rv_warungs.setHasFixedSize(true)

        refreshUI()
    }

    private fun refreshUI() {
        ApiMain().services.getAllWarung().enqueue(object :
            Callback<ResponseWarungs> {
            override fun onResponse(
                call: Call<ResponseWarungs>,
                response: Response<ResponseWarungs>
            ) {
                // if request success
                if (response.code()==200){
                    Log.i("MapsActivity", "Berhasil request");
                    Log.i("MapsActivity", response.body().toString());

                    var result = ArrayList<Warungs>()
                    response.body()?.warungs!!.forEach {
                        var pekanbaruLoc = Location("");
                        pekanbaruLoc.latitude = pekanbaru.latitude
                        pekanbaruLoc.longitude = pekanbaru.longitude

                        var warungLoc = Location("")
                        warungLoc.latitude = it.lat!!
                        warungLoc.longitude = it.lng!!

                        var jarak = pekanbaruLoc.distanceTo(warungLoc)
                        if (jarak<=radius){
                            it.jarak = jarak.toInt().toString()
                            result.add(it)
                        }
                    }

                    showWarungList(result, response.body()?.warungs!!)
                }
            }

            override fun onFailure(call: Call<ResponseWarungs>, t: Throwable) {
                // if request error
                Toast.makeText(applicationContext, "Terjadi kesalahan", Toast.LENGTH_LONG).show()
                t.printStackTrace();
                Log.e("MapsActivity", t.localizedMessage);
            }
        })
    }

    private fun showWarungList(warungs: List<Warungs>, warungsAll: List<Warungs>) {

        rv_warungs.layoutManager = LinearLayoutManager(this)
        val listWarungAdapter = ListWarungAdapter(warungs)
        rv_warungs.adapter = listWarungAdapter

        warungsAll.forEach{
            val position = it.lat?.let { it1 -> it.lng?.let { it2 -> LatLng(it1, it2) } }
            mMap.addMarker(position?.let { it1 -> MarkerOptions().position(it1).title(it.name).icon(bitmapDescriptorFromVector(applicationContext, R.drawable.ic_blue_marker)) })
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker println(it.id)
            println(it.name)near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        circle = mMap.addCircle(CircleOptions()
            .center(pekanbaru)
            .radius(radius.toDouble())
            .strokeWidth(10F)
            .strokeColor(Color.RED)
            .fillColor(Color.argb(32, 255, 0, 0))
            .clickable(false));


        // Add a marker in Pekanbaru and move the camera

        mMap.addMarker(MarkerOptions().position(pekanbaru).title("Pekanbaru City"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pekanbaru, 16.0f))
    }

    private fun bitmapDescriptorFromVector(
        context: Context,
        vectorResId: Int
    ): BitmapDescriptor? {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

}
